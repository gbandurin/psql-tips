    <div class="tip">
        The <code>\o |command</code> meta command will redirect all query results into the
	command specified.

	In that case, the entire remainder of the line is taken to be the
	command to execute, and neither variable interpolation nor backquote
	expansion are performed in it. The rest of the line is simply passed
	literally to the shell.
      <pre><code class="hljs bash">laetitia=# \o |grep -i 'bla'
laetitia=# select * from test limit 5;
laetitia=#   1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla</code></pre>This feature is available
at least since Postgres 7.1.
	</div>
